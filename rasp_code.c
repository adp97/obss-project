/**********************************************************
 *  INCLUDES
 *********************************************************/

#include <stdio.h>
#include <time.h>

#include "rasp_code.h"

/**********************************************************
 *  CONSTANTS
 *********************************************************/

#define MAX_TEMPERATURE 90.
#define MIN_TEMPERATURE -10.0 
#define AVG_TEMPERATURE 40.0 

/**********************************************************
 *  PUBLIC STATUS (GLOBAL VARIABLES)
 **********************************************************/

// boolean with the status of the heater
int heater_on = 0;
// boolean with the status of the sunlight
int sunlight_on = 0;
// Save the actual temperature of the ship
double temperature = 0.0;
// actual position of the ship
struct position position;

// next command message to be send
struct cmd_msg next_cmd_msg = {NO_CMD, 0};
// last response message received
struct res_msg last_res_msg = {NO_CMD, 0};

//---------------------------------------------------------------------------
//                           MAIN FUNCTIONS
//---------------------------------------------------------------------------


/**********************************************************
 *  Function: control_temperature
 *********************************************************/
void control_temperature () 
{
    // check if temperature is lower or higher
    if (temperature < AVG_TEMPERATURE) {
	// set heater
	heater_on = 1;
    } else if (temperature >= AVG_TEMPERATURE) {
	// unset heater
	heater_on = 0;
    }
}

/**********************************************************
 *  Function: send_cmd_msg
 *********************************************************/
void send_cmd_msg (enum command cmd)
{
    //No command
    if (cmd==NO_CMD) {
        next_cmd_msg.cmd=NO_CMD;
    }
    
    //Set heater
    else if (cmd==SET_HEAT_CMD) {
    	heater_on=1-heater_on;
    	next_cmd_msg.cmd=SET_HEAT_CMD;
    	next_cmd_msg.set_heater=heater_on;
    }

    //Read the light sensor
    else if (cmd==READ_SUN_CMD) {
    	next_cmd_msg.cmd=READ_SUN_CMD;
    }
    
    //Read the temperature
    else if (cmd==READ_TEMP_CMD) {
    	next_cmd_msg.cmd=READ_TEMP_CMD;
    }
    
    //Read the position
    else if (cmd==READ_POS_CMD) {
    	next_cmd_msg.cmd=READ_POS_CMD;
    };
    
    //printf("send_cmd_msg: %d\n",sizeof(struct cmd_msg));
}

/**********************************************************
 *  Function: recv_res_msg
 *********************************************************/
void recv_res_msg ()
{
    //No command
    if (last_res_msg.cmd == NO_CMD) {
        printf("Answer: NO_CMD\n");
        
    //Set heater
    } else if (last_res_msg.cmd == SET_HEAT_CMD) {
        printf("Answer: SET_HEAT_CMD\n");
        printf("Status: %d\n",last_res_msg.status);
        printf("Heater: %d\n",heater_on);
        
    //Read the sunlight sensor
    } else if (last_res_msg.cmd == READ_SUN_CMD) {
        printf("Answer: READ_SUN_CMD\n");
        printf("Status: %d\n",last_res_msg.status);
        printf("Sun Sensor: %d\n",last_res_msg.data.sunlight_on);

    //Read the temperature
    } else if (last_res_msg.cmd == READ_TEMP_CMD) {
        printf("Answer: READ_TEMP_CMD\n");
        printf("Status: %d\n",last_res_msg.status);
        printf("Temperature: %f\n",last_res_msg.data.temperature);

    //Read the position
    } else if (last_res_msg.cmd == READ_POS_CMD) {
        printf("Answer: READ_POS_CMD\n");
        printf("Status: %d\n",last_res_msg.status);
        printf("Position: %f, %f, %f\n",last_res_msg.data.position.x,
                                        last_res_msg.data.position.y,
                                        last_res_msg.data.position.z);
    }

    //set response to no command
    last_res_msg.cmd = NO_CMD;
}
