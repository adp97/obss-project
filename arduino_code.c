/**********************************************************
 *  INCLUDES
 *********************************************************/

#include <stdio.h>
#include <time.h>
#include <math.h>

#include "arduino_code.h"

/**********************************************************
 *  CONSTANTS
 *********************************************************/

#define NS_PER_S  1000000000

#define SHIP_SPECIFC_HEAT 0.9
#define SHIP_MASS 10.0  // Kg
#define HEATER_POWER 150.0 // J/sec
#define SUNLIGHT_POWER 50.0 // J/sec
#define HEAT_POWER_LOSS -100.0 // J/sec

#define ORBIT_POINTS_SIZE 20
#define ORBIT_TIME 300.0  //sec

/**********************************************************
 *  PUBLIC STATUS (GLOBAL VARIABLES)
 **********************************************************/

// boolean with the status of the heater
int heater_on = 0;
// boolean with the status of the sunlight
int sunlight_on = 0;
// Save the actual temperature of the ship
double temperature = 0.0;
// save the last time temperature was computed 
double time_temperature = 0.0;
// inital time of the orbit
double init_time_orbit = 0.0;
// actual position of the ship
struct position position;

// last command message received
struct cmd_msg last_cmd_msg = {NO_CMD, 0};
// next response message to be send
struct res_msg next_res_msg = {NO_CMD, 0};

/**********************************************************
 *  PRIVATE STATUS (STATIC GLOBAL VARIABLES)
 **********************************************************/

// position data for the orbit
static struct position orbit_points[ORBIT_POINTS_SIZE] = {
    {3000.00, 0, 12000.0},
    {2853.169548885460, 1854.101966249680, 11412.678195541800},
    {2427.050983124840, 3526.711513754840, 9708.203932499370},
    {1763.355756877420, 4854.101966249680, 7053.423027509680},
    {927.050983124842, 5706.339097770920, 3708.203932499370},
    {0.0, 6000.0, 0.0},
    {-927.050983124842, 5706.339097770920, -3708.203932499370},
    {-1763.355756877420, 4854.101966249680, -7053.423027509680},
    {-2427.050983124840, 3526.711513754840, -9708.203932499370},
    {-2853.169548885460, 1854.101966249680, -11412.678195541800},
    {-3000.0, 0.0, -12000.0},
    {-2853.169548885460, -1854.101966249690, -11412.678195541800},
    {-2427.050983124840, -3526.711513754840, -9708.203932499370},
    {-1763.355756877420, -4854.101966249680, -7053.423027509680},
    {-927.050983124843, -5706.339097770920, -3708.203932499370},
    {0.0, -6000.0, 0.0},
    {927.050983124842, -5706.339097770920, 3708.203932499370},
    {1763.355756877420, -4854.101966249690, 7053.423027509680},
    {2427.050983124840, -3526.711513754840, 9708.203932499370},
    {2853.169548885460, -1854.101966249690, 11412.678195541800}};

//---------------------------------------------------------------------------
//                           AUXILIAR FUNCTIONS 
//---------------------------------------------------------------------------

/**********************************************************
 *  Function: getClock
 *********************************************************/
double getClock()
{
    struct timespec tp;
    double reloj;

    clock_gettime (CLOCK_REALTIME, &tp);
    reloj = ((double)tp.tv_sec) +
	    ((double)tp.tv_nsec) / ((double)NS_PER_S);

    return (reloj);
}

//---------------------------------------------------------------------------
//                           MAIN FUNCTIONS
//---------------------------------------------------------------------------

/**********************************************************
 *  Function: get_temperature
 *********************************************************/
// Declaration of all the variables
int epoch_time; 
int energy_sun;
int energy_heater;

void get_temperature ()
{
    double time=getClock()-time_temperature; // Time difference

    //if (time>=1) {
        double power = HEAT_POWER_LOSS; // Always active this loss
        
        // Enable HEATER_POWER if heater is on, and 0 if it is off
        if (heater_on==0){
            energy_heater =0;
        } else
        {
            energy_heater = HEATER_POWER;
        }

        // Enable SUNLIGHT_POWER if its facing the Sun, and 0 if not
        if (sunlight_on==0) {
            energy_sun =0;
        } else
        {
            energy_sun=SUNLIGHT_POWER;
        }
        
        int energy = (HEAT_POWER_LOSS  + energy_heater + energy_sun)*time ;

        int new_temp = energy / (SHIP_SPECIFC_HEAT * SHIP_MASS)+ temperature;
        temperature = new_temp;

        time_temperature=getclock(); // Update of the last time
    //}     

}

/**********************************************************
 *  Function: get_position
 *********************************************************/
void get_position ()
{
    double time=getClock(); // Time difference
    time = remainder(time,ORBIT_TIME);

    int index = ORBIT_POINTS_SIZE * time / ORBIT_TIME;
    struct position first_ref_pos =  orbit_points[index];
    struct position second_ref_pos = orbit_points[(index+1)%ORBIT_POINTS_SIZE]; 

    double offset_ratio = (time/(ORBIT_TIME/ORBIT_POINTS_SIZE))-index;
    struct position new_coord;
    new_coord.x = first_ref_pos.x  *(1-offset_ratio)+second_ref_pos.x*offset_ratio; 
    new_coord.y = first_ref_pos.y  *(1-offset_ratio)+second_ref_pos.y*offset_ratio; 
    new_coord.z = first_ref_pos.z  *(1-offset_ratio)+second_ref_pos.z*offset_ratio; 

    time=getclock(); // Update of the last time 
}

/**********************************************************
 *  Function: exec_cmd_msg
 *********************************************************/

void exec_cmd_msg ()
{
    if (last_cmd_msg.cmd==SET_HEAT_CMD) {
        //Task
        heater_on=last_cmd_msg.set_heater;
        
        //Response
        next_res_msg.cmd=SET_HEAT_CMD;
        next_res_msg.status=1;
    }
    
    else if (last_cmd_msg.cmd==READ_SUN_CMD) {
        //Task
        //function to read the sensor
        
        //Response
        next_res_msg.cmd=READ_SUN_CMD;
        next_res_msg.status=1;
        next_res_msg.data.sunlight_on=sunlight_on;
        }
        
    else if (last_cmd_msg.cmd==READ_TEMP_CMD) {
        //Task
        get_temperature();
        
        //Response
        next_res_msg.cmd=READ_TEMP_CMD;
        next_res_msg.status=1;
        next_res_msg.data.temperature=temperature;
        }
        
    else if (last_cmd_msg.cmd==READ_POS_CMD) {
        //Task
        get_position();
        
        //Response
        next_res_msg.cmd=READ_POS_CMD;
        next_res_msg.status=1;
        next_res_msg.data.position=position;
        }
        
    else {};
    
    //Resetting the last command received
    last_cmd_msg.cmd=NO_CMD;
}      