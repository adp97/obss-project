/**********************************************************
 *  INCLUDES
 *********************************************************/
#include <gtest/gtest.h>
#include <unistd.h>
#include <stdio.h>

extern "C" {
#include "arduino_code.h"
#include "arduino_code.c"
}

// This is how it must look at the end
TEST(exec_cmd_msg,heater_test){
    time_temperature = getClock();
    last_cmd_msg.cmd = READ_TEMP_CMD;
    exec_cmd_msg();
    
    double prev_temp = temperature;
    printf("Previous Temperature = %f ºC \n",prev_temp);
    
    last_cmd_msg.cmd = SET_HEAT_CMD;
    last_cmd_msg.set_heater = TRUE ; 
    exec_cmd_msg();
    EXPECT_EQ(TRUE, next_res_msg.status);


    sleep(5)
    last_cmd_msg.cmd = READ_TEMP_CMD;
    exec_cmd_msg();

    double next_temp = temperature;
    printf("Temperature after 5sec = %f ºC \n",next_temp);

}

/**********************************************************
 *  Funtion: main
 *********************************************************/

int main(int argc, char **argv) 
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

